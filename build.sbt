scalaVersion := "2.11.8"

enablePlugins(AndroidApp)
android.useSupportVectors

versionCode := Some(1)
version := "0.1-SNAPSHOT"

instrumentTestRunner :=
  "android.support.test.runner.AndroidJUnitRunner"

platformTarget := "android-25"
minSdkVersion in Android := "21"
dexMaxHeap in Android := "2g"

javacOptions in Compile ++= "-source" :: "1.7" :: "-target" :: "1.7" :: Nil

val scafi_core  = "it.unibo.apice.scafiteam" %% "scafi-core"  % "0.2.0"
val scafi_simulator = "it.unibo.apice.scafiteam" %% "scafi-simulator" % "0.2.0"

libraryDependencies ++=
  "com.android.support" % "appcompat-v7" % "24.0.0" ::
  "com.android.support.test" % "runner" % "0.5" % "androidTest" ::
  "com.android.support.test.espresso" % "espresso-core" % "2.2.2" % "androidTest" ::
  Nil

libraryDependencies += "org.glassfish.jersey.core" % "jersey-client" % "2.25.1"
libraryDependencies += "com.android.support" % "multidex" % "1.0.0"
libraryDependencies += "com.lambdaworks" %% "jacks" % "2.3.3"

proguardScala in Android := false
useProguard in Android := false
dexMulti in Android := true

packagingOptions in Android := PackagingOptions(excludes = Seq("META-INF/NOTICE", "META-INF/LICENSE"))