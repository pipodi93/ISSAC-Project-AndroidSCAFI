package it.pipodi.issacscafi

import java.util.{Timer, TimerTask}
import javax.ws.rs.client.{ClientBuilder, Entity, WebTarget}
import javax.ws.rs.core.{MediaType, Response}

import android.content.Context
import android.location.{Location, LocationListener, LocationManager}
import android.os.{Bundle, Handler}
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.view.View.OnClickListener
import android.widget._
import org.json.{JSONArray, JSONObject}
import it.unibo.scafi.incarnations.BasicSimulationIncarnation._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success}


class MainActivity extends AppCompatActivity {

    private var deviceID: String = _
    private val REST_URI = "http://192.168.0.31:8080/issacrest"
    private val SHARED_NAME = "issacscafi_prefs"
    private val client = ClientBuilder.newClient
    private var target: WebTarget = _
    private var exportsMap: Map[Int, ExportImpl] = Map()
    private var nbrMap: Map[Int, Any] = Map()
    private val factory: EngineFactory = new EngineFactory
    private var currentPosition: Location = new Location("")
    private var isSource: Boolean = false
    private var rotationAngle: Float = _
    private var navigationArrow: ImageView = _
    private var registerButton: Button = _
    private var context: Context = _
    private val RETRIEVE_INTERVAL: Long = 30 * 1000
    private val AGGREGATE_INTERVAL: Long = 5 * 1000
    private var timerRetrieve: Timer = null
    private val handlerRetrieve = new Handler()
    private var timerAggregate: Timer = null
    private val handlerAggregate = new Handler()

    override def onCreate(savedInstanceState: Bundle) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main)
        context = this
        target = client.target(REST_URI)
        val toggle = findViewById(R.id.toggle_source).asInstanceOf[ToggleButton]
        registerButton = findViewById(R.id.register_button).asInstanceOf[Button]
        val startButton = findViewById(R.id.start_button).asInstanceOf[Button]
        navigationArrow = findViewById(R.id.arrow).asInstanceOf[ImageView]
        getGPSCoords()
        if (getSharedPreferences(SHARED_NAME, Context.MODE_PRIVATE).contains("deviceID")) {
            deviceID = getSharedPreferences(SHARED_NAME, Context.MODE_PRIVATE).getString("deviceID", "")
            startService()
        }
        toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            override def onCheckedChanged(buttonView: CompoundButton, isChecked: Boolean): Unit = {
                if (isChecked) {
                    isSource = true
                    getSharedPreferences(SHARED_NAME, Context.MODE_PRIVATE).edit().putBoolean("isSource", true).apply()
                }
                else {
                    isSource = false
                    getSharedPreferences(SHARED_NAME, Context.MODE_PRIVATE).edit().putBoolean("isSource", false).apply()
                }
            }
        })
        registerButton.setOnClickListener(new OnClickListener {
            override def onClick(view: View): Unit = {
                if (getSharedPreferences(SHARED_NAME, Context.MODE_PRIVATE).getBoolean("registered", false)) {
                    Toast.makeText(context, "The device is already registered.", Toast.LENGTH_LONG).show()
                } else {
                    val registrationFuture = Future {
                        registerDevice(currentPosition)
                    }
                    registrationFuture.onComplete {
                        case Success(value) => {
                            val registrationResponseObj: JSONObject = new JSONObject(value.readEntity(classOf[String]))
                            val deviceIDFromServer = registrationResponseObj.getString("id")
                            getSharedPreferences(SHARED_NAME, Context.MODE_PRIVATE).edit().putString("deviceID", deviceIDFromServer).apply()
                            getSharedPreferences(SHARED_NAME, Context.MODE_PRIVATE).edit().putBoolean("registered", true).apply()
                            deviceID = deviceIDFromServer
                            registerButton.setClickable(false)
                            startService()
                        }
                        case Failure(e) => Log.i("REGISTRATION", e.toString)
                    }
                }
            }
        })
        startButton.setOnClickListener(new OnClickListener {
            override def onClick(view: View): Unit = {
                if (getSharedPreferences(SHARED_NAME, Context.MODE_PRIVATE).contains("deviceID")) {
                    deviceID = getSharedPreferences(SHARED_NAME, Context.MODE_PRIVATE).getString("deviceID", "")
                    startAggregateComputation()
                } else {
                    Toast.makeText(context, "You need to register the device first.", Toast.LENGTH_LONG).show()
                }
            }
        })
    }

    def getGPSCoords(): Unit = {
        val locationManager = this.getSystemService(Context.LOCATION_SERVICE).asInstanceOf[LocationManager]
        val locationListener = new LocationListener() {
            def onLocationChanged(location: Location): Unit = {
                currentPosition = location
                val locationJSON: JSONObject = new JSONObject
                locationJSON.put("lat", location.getLatitude)
                locationJSON.put("lng", location.getLongitude)
                getSharedPreferences(SHARED_NAME, Context.MODE_PRIVATE).edit().putString("locationJSON", locationJSON.toString).apply()
            }

            override def onStatusChanged(provider: String, status: Int, extras: Bundle): Unit = {
            }

            override def onProviderEnabled(provider: String): Unit = {
            }

            override def onProviderDisabled(provider: String): Unit = {
            }
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener)
    }

    def executeAggregateProgram(location: Location): Unit = {
        val exportsString: String = getSharedPreferences(SHARED_NAME, Context.MODE_PRIVATE).getString("neighexports", "")
        Log.i("NEIGHBOURSEXPORTS", exportsString)
        val responseArray: JSONArray = new JSONArray(exportsString)
        for (i <- 0 until responseArray.length()){
            val deviceJSON: JSONObject = responseArray.getJSONObject(i)
            val deviceIDServer: String = deviceJSON.getString("id")
            val distance = deviceJSON.getString("dist").toDouble
            val deviceValue: String = deviceJSON.getString("value")
            var export: ExportImpl = new ExportImpl
            if (deviceValue.equals("")) {
                export = factory.emptyExport()
            } else {
                export = createExportFromJSONArray(new JSONArray(deviceValue))
            }
            nbrMap += deviceIDServer.toInt -> distance
            exportsMap += deviceIDServer.toInt -> export
        }
        Log.i("NBRMAP", nbrMap.toString())
        Log.i("EXPORTSMAP", exportsMap.toString())
        val ctx = factory.context(
            selfId = deviceID.toInt,
            exports = exportsMap,
            nbsens = Map(NBR_RANGE_NAME -> nbrMap))
        Log.i("CONTEXT", ctx.toString())
        val deviceExport: ExportImpl = SteeringProgram.apply(ctx)
        Log.i("AGGREGATEEXPORT", deviceExport.toString())
        val exportMap = deviceExport.getMap()
        val exportJSONArray: JSONArray = new JSONArray()
        for ((p, v) <- exportMap){
            val obj: JSONObject = new JSONObject()
            obj.put("path", parsePathString(p.toString))
            if (v.isInstanceOf[Double] && v.asInstanceOf[Double].isInfinity) {
                obj.put("result", "Inf")
            } else if (v.isInstanceOf[Double] || v.isInstanceOf[Int]) {
                obj.put("result", v.toString)
            } else {
                obj.put("result", v)
            }
            exportJSONArray.put(obj)
        }
        Log.i("EXPORTJSON", exportJSONArray.toString())
        val postFuture = Future{
            postDevice(deviceID, exportJSONArray.toString(), location)
        }
        postFuture.onComplete{
            case Success(value) => {
                Log.i("POST", value.readEntity(classOf[Boolean]).toString)
                navigationArrow.setRotation(0)
                navigationArrow.setRotation(rotationAngle)
            }
            case Failure(e) => Log.i("POST", e.printStackTrace().toString)
        }
    }

    def parsePathString(string: String): JSONObject ={
        val jSONObject = new JSONObject()
        val parsedString = string.replace("P:/", "")
        if (!parsedString.equals("")){
            jSONObject.put("key", parsedString)
        } else{
            jSONObject.put("key", "")
        }
        jSONObject
    }

    def parseTupleString(tupleString: String): Tuple3[Double, Int, String] ={
        val plessString = tupleString.replace("(", "").replace(")", "")
        val tupleValues: Array[String] = plessString.split(",")
        (tupleValues(0).toDouble, tupleValues(1).toInt, tupleValues(2))
    }

    def createExportFromJSONArray(jSONArray: JSONArray): ExportImpl = {
        val export: ExportImpl = factory.emptyExport()
        for (i <- 0 until jSONArray.length ){
            val jSONObject: JSONObject = jSONArray.getJSONObject(i)
            var result: Any = jSONObject.get("result")
            val path: Path = createPathFromJSONObj(new JSONObject(jSONObject.getString("path")))
            if (result.equals("Inf")) {
               export.put(path, Double.PositiveInfinity)
            } else if (scala.util.Try(result.toString.toInt).isSuccess) {
                export.put(path, result.toString.toInt)
            } else if (scala.util.Try(result.toString.toDouble).isSuccess) {
                export.put(path, result.toString.toDouble)
            } else {
                export.put(path, if (!result.toString.equals("()") )parseTupleString(result.toString) else result)
            }
        }
        Log.i("CREATEDEXPORT", export.toString())
        export

    }

    def createPathFromJSONObj(jSONObject: JSONObject): Path = {
        var path: Path = factory.emptyPath()
        val string = jSONObject.getString("key")
        val pathStrings: Array[String] = string.split("/")
        for (stringElem <- pathStrings) {
            if (stringElem.length > 0) {
                val index: String = stringElem.substring(stringElem.length - 2, stringElem.length - 1)
                val tempString = stringElem.substring(0, stringElem.length - 3).trim
                tempString match {
                    case "Rep" => {
                        path = path.push(Rep[Any](index.toInt))
                    }
                    case "Nbr" => {
                        path = path.push(Nbr[Any](index.toInt))
                    }
                    case "FoldHood" => {
                        path = path.push(FoldHood[Any](index.toInt))
                    }
                }
            }
        }
        path
    }

    def postDevice(id: String, value: String, deviceLocation: Location): Response = {
        val obj: JSONObject = new JSONObject
        obj.put("key", id)
        obj.put("value", value)
        obj.put("lat", deviceLocation.getLatitude)
        obj.put("lng", deviceLocation.getLongitude)
        target.path("export").request.header("payload", obj.toString).post(Entity.entity("", MediaType.APPLICATION_JSON))
    }

    def registerDevice(location: Location): Response = {
        val obj: JSONObject = new JSONObject
        obj.put("lat", location.getLatitude)
        obj.put("lng", location.getLongitude)
        target.path("export/register").request.header("payload", obj.toString).post(Entity.entity("", MediaType.APPLICATION_JSON))
    }

    def startService(): Unit = {
        if (timerRetrieve != null) {
            timerRetrieve.cancel()
        }
        else {
            timerRetrieve = new Timer()
        }
        Log.i("SERVICE", "DeviceID: " + deviceID)
        timerRetrieve.scheduleAtFixedRate(new RetrievingTask, 0, RETRIEVE_INTERVAL)
    }

    def startAggregateComputation(): Unit = {
        if (timerAggregate != null) {
            timerAggregate.cancel()
        } else {
            timerAggregate = new Timer()
        }
        timerAggregate.scheduleAtFixedRate(new AggregateComputationTask, 0, AGGREGATE_INTERVAL)
    }

    object SteeringProgram extends AggregateProgram {
        def main = steering(isSource)

        def steering(source: Boolean): Unit = {
            val g = classicGradient(source)
            val p: String = currentPosition.getLatitude + ";" + currentPosition.getLongitude
            val q = minHoodPLoc((g, mid, p))(nbr{ (g, mid, p) })._3
            Log.i("STEERING", "p: " + p.toString + " | q: " + q.toString)
            val coordStrings: Array[String] = q.split(";")
            val nearestLocation : Location = new Location("")
            nearestLocation.setLatitude(coordStrings(0).toDouble)
            nearestLocation.setLongitude(coordStrings(1).toDouble)
            rotationAngle = currentPosition.bearingTo(nearestLocation)
            Log.i("ROTATION", rotationAngle.toString)
        }

        def classicGradient(source: Boolean): Double = rep(Double.PositiveInfinity){ distance =>
            mux(source){ 0.0 }{
                minHoodPlus(nbr{distance} + nbrRange)
            }
        }

        def minHoodPLoc[A](default: A)(expr: => A)(implicit poglb: Ordering[A]): A = {
            import scala.math.Ordered.orderingToOrdered
            val ordering = implicitly[Ordering[A]]
            foldhoodPlus[A](default)((x, y) => if(x <= y) x else y){expr}
        }

        implicit def tupleOrd[A:Ordering, B:Ordering, C]: Ordering[(A,B,C)] = new Ordering[(A,B,C)] {
            import scala.math.Ordered.orderingToOrdered
            override def compare(x: (A, B, C), y: (A, B, C)): Int = (x._1,x._2).compareTo((y._1,y._2))
        }
        def nbrRange(): Double = nbrvar[Double](NBR_RANGE_NAME)
        // NBR_RANGE_NAME = "nbrRange"
    }


    class RetrievingTask extends TimerTask{
        override def run(): Unit = {
            handlerRetrieve.post(new Runnable {
                override def run(): Unit = {
                    val getNeighborsExportFuture = Future{
                        val location: Location = new Location("")
                        val locationString: String = getSharedPreferences(SHARED_NAME, Context.MODE_PRIVATE).getString("locationJSON", "")
                        val locationJSON: JSONObject = new JSONObject(locationString)
                        Log.i("SERVICE", "Lat: " + locationJSON.getDouble("lat") + "Lng: " + locationJSON.getDouble("lng"))
                        location.setLatitude(locationJSON.getDouble("lat"))
                        location.setLongitude(locationJSON.getDouble("lng"))
                        getNeighborsExports(deviceID, location)
                    }
                    getNeighborsExportFuture.onComplete {
                        case Success(value) => {
                            val string: String = value.readEntity(classOf[String])
                            Log.i("SERVICE", "Exports: " + string)
                            getSharedPreferences(SHARED_NAME, Context.MODE_PRIVATE).edit().putString("neighexports", string).apply()
                        }
                        case Failure(e) => Log.i("EXPORT", e.printStackTrace().toString)
                    }
                }
            })
        }
    }

    class AggregateComputationTask extends TimerTask {
        override def run(): Unit = {
            handlerAggregate.post(new Runnable {
                override def run(): Unit = {
                    executeAggregateProgram(currentPosition)
                }
            })
        }
    }

    def getNeighborsExports(id: String, deviceLocation: Location): Response = {
        val obj: JSONObject = new JSONObject
        obj.put("key", "" + id)
        obj.put("lat", deviceLocation.getLatitude)
        obj.put("lng", deviceLocation.getLongitude)
        target.path("export").request.header("payload", obj.toString).get()
    }
}